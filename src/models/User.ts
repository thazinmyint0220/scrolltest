export class User {
  private _userId: string = "";

  get userId(): string {
    return this._userId;
  }

  set userId(value: string) {
    this._userId = value;
  }

  clone(): User {
    const cloneUser = new User();
    cloneUser.userId = this._userId
    return cloneUser;
  }
}
